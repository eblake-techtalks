# -*- shell-script -*-

# Colour ls.
if [ -f /etc/profile.d/colorls.sh ]; then . /etc/profile.d/colorls.sh; fi

# Fancy prompt colours (see
# https://wiki.archlinux.org/index.php/Color_Bash_Prompt)
titlecol=$'\e[1;37;41m'    ;# colour for the title
promptcol=$'\e[0;32m'      ;# colour for the prompt
commandcol=$'\e[1;31m'     ;# colour for the typed command
outputcol=$'\e[0m'         ;# colour for command output

export PS1="\n\[$promptcol\]\$ \[$commandcol\]"

trap 'echo -ne "$outputcol"' DEBUG

# Load key bindings (if any).
bind -f $talkdir/bindings

# Same as the banner function, but we cannot reuse it.
printf "\e]0;$title\a"
printf "%s   %s   %s\\n" $titlecol "$title" $outputcol
